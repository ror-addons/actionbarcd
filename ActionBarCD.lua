-- ABCD (ActionBarCoolDown) Mod by Mr. Novoc (novo@ilsken.net)
-- Thanks to Aiiane, TheWarWiki.com and #waruidev @ Freenode

-- LabelSetFont( labelName, font, linespacing )
-- font_chat_text
-- font_clear_tiny -- Myriad Pro - Very Small
-- font_clear_small -- Myriad Pro - Small
-- font_clear_medium -- Myriad Pro - Medium
-- font_clear_large -- Myriad Pro - Large
-- font_clear_small_bold -- Myriad Pro SemiExt - Small
-- font_clear_medium_bold -- Myriad Pro SemiExt - Medium
-- font_clear_large_bold -- Myriad Pro SemiExt - Large
-- font_default_text_small -- Age of Reckoning - Small
-- font_default_text -- Age of Reckoning - Medium
-- font_default_text_large -- Age of Reckoning - Large
-- font_default_text_huge -- Age of Reckoning - Huge
-- font_journal_text_small -- Cronos Pro - Small
-- font_journal_text -- Cronos Pro - Medium

-- WindowUtils.FONT_DEFAULT_TEXT_LINESPACING = 20
-- WindowUtils.FONT_DEFAULT_SUB_HEADING_LINESPACING = 32

-- ABCD.fade
-- Graphical fading of actionbutton-background On/Off (1/0)

-- ABCD.flash
-- Graphical 'flash' when cooldown is out On/Off (1/0)

-- ABCD.font
-- 0 = font_default_text
-- 1 = font_default_text_small
-- 2 = font_default_text_large
-- 3 = font_clear_small
-- 4 = font_clear_medium
-- 5 = font_clear_large
-- 6 = font_clear_small_bold
-- 7 = font_clear_medium_bold
-- 8 = font_clear_large_bold

-- ABCD.fontsize
-- 0 = tiny
-- 1 = small
-- 2 = medium
-- 3 = large
-- 4 = huge

-- ABCD.text
-- Display time-text showing how much cooldown-time is left On/Off (1/0)

ActionBarCD = {}

function ActionBarCD.Initialize()

	-- ABCD Default settings
	if not ABCD then
		ABCD = {
			fade = 1,
			flash = 1,
			font = 1,
			glow = 1,
			text = 1,
			sec = 0,
		}
	end

	LibSlash.RegisterSlashCmd("abcd", function(args) ActionBarCD.SlashHandler(args) end)

	-- Replace the default UpdateCooldownAnimation function for ActionButtons with our own
	ActionButton.UpdateCooldownAnimation = function(self, timeElapsed, updateCooldown)
		local COOLDOWN		= 2
		local COOLDOWN_TIMER	= 3
		local FLASH_ANIM	= 4
		local ACTIVE_ANIM       = 5

		-- self.m_Windows[ACTIVE_ANIM]:SetTintColor(255, 0, 0)
		-- self.m_Windows[FLASH_ANIM]:SetTintColor(255, 0, 0)
		-- self.m_Windows[COOLDOWN]:SetTintColor(255, 0, 0)
		-- self.m_Windows[COOLDOWN_TIMER]:SetTintColor(255, 0, 0)

		if (ABCD.glow == 0) then
			self.m_Windows[ACTIVE_ANIM]:SetAlpha (0)
		else
			self.m_Windows[ACTIVE_ANIM]:SetAlpha (1)
		end

		local INITIAL_COOLDOWN_ALPHA	= ABCD.fade
		local BASE_COOLDOWN_ALPHA	= 0

		local cooldownFrame	= self.m_Windows[COOLDOWN]
		local timerFrame	= self.m_Windows[COOLDOWN_TIMER]
		local updateCooldown	= updateCooldown or self.m_RequiresFullUpdate

		if (updateCooldown) then
			self.m_Cooldown, self.m_MaxCooldown = GetHotbarCooldown (self:GetSlot ())
		end

		if ((self.m_Cooldown > 0) and (self.m_MaxCooldown > 0)) then 
			local currentSeconds    = math.floor (self.m_Cooldown)
			local updateLabel       = false

			self.m_Cooldown = self.m_Cooldown - timeElapsed

			if (ABCD.text == 1) then
				if (math.floor (self.m_Cooldown) < currentSeconds) then
					updateLabel = true
				end

				if (updateLabel or updateCooldown) then
					timerFrame:SetDimensions(100,100)

					if (ABCD.font == 1) then
						timerFrame:SetFont ("font_default_text_huge", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
					elseif (ABCD.font == 2) then
						timerFrame:SetFont ("font_heading_large", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
					else
						timerFrame:SetFont ("font_default_text", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
					end

					local labelTime = TimeUtils.FormatSeconds(self.m_Cooldown, false)
					if sec == 1 then
						local labelTime = TimeUtils.FormatSeconds(self.m_Cooldown, true)
					end

					if (ABCD.color == 1) then
						timerFrame:SetTextColor (255, 0, 0)
					elseif (ABCD.color == 2) then
						timerFrame:SetTextColor (0, 255, 0)
					elseif (ABCD.color == 3) then
						timerFrame:SetTextColor (0, 255, 255)
					else
						timerFrame:SetTextColor (255, 255, 0)
					end

					timerFrame:SetText (labelTime)
				end
			end

			if (ABCD.fade == 1) then
				local percent   = self.m_Cooldown / self.m_MaxCooldown
				local newAlpha  = (INITIAL_COOLDOWN_ALPHA * percent) + BASE_COOLDOWN_ALPHA
				cooldownFrame:SetAlpha (newAlpha)
			else
				cooldownFrame:SetAlpha (0)
			end

			cooldownFrame:Show (true)
		elseif (self.m_Cooldown <= 0 and cooldownFrame:IsShowing ()) then
			cooldownFrame:SetAlpha (INITIAL_COOLDOWN_ALPHA + BASE_COOLDOWN_ALPHA)
			cooldownFrame:Show (false)

			self.m_Cooldown     = 0
			self.m_MaxCooldown  = 0

			if (ABCD.flash == 1) then
				self.m_Windows[FLASH_ANIM]:StartAnimation (0, false, true, 0)
			end
		end
	end

	d("ABCD Mod loaded successfully!")
	TextLogAddEntry("Chat", 3 , StringToWString("ABCD Mod loaded successfully!"))
end

local function chat(txt)
	ChatWindow.Print(towstring(txt))
end

function ActionBarCD.SlashHandler(args)
	local opt, val = args:match("([a-z0-9]+)[ ]?(.*)")

	if not opt then
		chat("ActionBarCoolDown Mod commands:")
		chat("color (0-3) -- Color of the cooldown-time text. 0=yellow,1=red,2=green,3=teal")
		chat("fade (on/off) -- Graphical fading of actionbutton-background On/Off")
		chat("flash (on/off) -- Graphical 'flash' when cooldown is out On/Off")
		chat("font (0-2) -- 0 is default, 1 is big, 2 is huge")
		chat("glow (on/off) -- Graphical glowing around the action-button when active On/Off")
		chat("sec (on/off) -- Wheter to have the 's' after number of seconds on button On/Off")
		chat("text (on/off) -- Display text showing how much cooldown-time is left On/Off")
	elseif opt == "color" then
		if val == "0" then
			ABCD.color = 0
			chat("ABCD.color is now Yellow")
		elseif val == "1" then
			ABCD.color = 1
			chat("ABCD.color is now Red")
		elseif val == "2" then
			ABCD.color = 2
			chat("ABCD.color is now Green")
		elseif val == "3" then
			ABCD.color = 3
			chat("ABCD.color is now Teal")
		else
			if ABCD.color == 0 then
				chat("ABCD.color is Yellow")
			elseif ABCD.color == 1 then
				chat("ABCD.color is Red")
			elseif ABCD.color == 2 then
				chat("ABCD.color is Green")
			elseif ABCD.color == 3 then
				chat("ABCD.color is Teal")
			end
		end
	elseif opt == "fade" then
		if val == "on" then
			ABCD.fade = 1
			chat("ABCD.fade is now ON")
		elseif val == "off" then
			ABCD.fade = 0
			chat("ABCD.fade is now OFF")
		else
			if ABCD.fade == 1 then
				chat("ABCD.fade is ON")
			else
				chat("ABCD.fade is OFF")
			end
		end
	elseif opt == "flash" then
		if val == "on" then
			ABCD.flash = 1
			chat("ABCD.flash is now ON")
		elseif val == "off" then
			ABCD.flash = 0
			chat("ABCD.flash is now OFF")
		else
			if ABCD.flash == 1 then
				chat("ABCD.flash is ON")
			else
				chat("ABCD.flash is OFF")
			end
		end
	elseif opt == "font" then
		if val == "0" then
			ABCD.font = 0
			chat("ABCD.font is now 'Age of Reckoning, Medium'")
		elseif val == "1" then
			ABCD.font = 1
			chat("ABCD.font is now 'Age of Reckoning, Small'")
		elseif val == "2" then
			ABCD.font = 2
			chat("ABCD.font is now 'Age of Reckoning, Large'")
		else
			if ABCD.font == 0 then
				chat("ABCD.font is 'Age of Reckoning, Medium'")
			elseif ABCD.font == 1 then
				chat("ABCD.font is 'Age of Reckoning, Small'")
			elseif ABCD.font == 2 then
				chat("ABCD.font is 'Age of Reckoning, Large'")
			end
		end
	elseif opt == "glow" then
		if val == "on" then
			ABCD.glow = 1
			chat("ABCD.glow is now ON")
		elseif val == "off" then
			ABCD.glow = 0
			chat("ABCD.glow is now OFF")
		else
			if ABCD.glow == 1 then
				chat("ABCD.glow is ON")
			else
				chat("ABCD.glow is OFF")
			end
		end
	elseif opt == "sec" then
		if val == "on" then
			ABCD.sec = 1
			chat("ABCD.sec is now ON")
		elseif val == "off" then
			ABCD.sec = 0
			chat("ABCD.sec is now OFF")
		else
			if ABCD.sec == 1 then
				chat("ABCD.sec is ON")
			else
				chat("ABCD.sec is OFF")
			end
		end
	elseif opt == "text" then
		if val == "on" then
			ABCD.text = 1
			chat("ABCD.text is now ON")
		elseif val == "off" then
			ABCD.text = 0
			chat("ABCD.text is now OFF")
		else
			if ABCD.text == 1 then
				chat("ABCD.text is ON")
			else
				chat("ABCD.text is OFF")
			end
		end
	end
end
