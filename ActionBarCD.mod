<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="ActionBarCD" version="0.2" date="17/09/2008" >

		<Author name="Novoc" email="novo@ilsken.net" />
		<Description text="ABCD Mod lets you customize the cooldown-display on actionbar-buttons." />

		<Dependencies>
			<Dependency name="LibSlash" />
			<Dependency name="EA_ActionBars" />
		</Dependencies>

		<Files>
			<File name="ActionBarCD.lua" />
		</Files>

		<SavedVariables>
			<SavedVariable name="ABCD" />
		</SavedVariables>

		<OnInitialize>
			<CallFunction name="ActionBarCD.Initialize" />
		</OnInitialize>

	</UiMod>
</ModuleFile>
